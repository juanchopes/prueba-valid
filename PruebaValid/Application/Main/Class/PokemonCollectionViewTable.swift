//
//  PokemonCollectionViewTable.swift
//  PruebaValid
//
//  Created by Juan martinez on 20/04/20.
//  Copyright © 2020 Juan Martinez. All rights reserved.
//

import Foundation
import UIKit

class PokemonCollectionViewTable: UITableViewCell{
    
    @IBOutlet weak var img_pokemon: UIImageView!
    @IBOutlet weak var img_type1: UIImageView!
    @IBOutlet weak var img_type2: UIImageView!
    @IBOutlet weak var txt_name: UILabel!
    @IBOutlet weak var txt_id: UILabel!
}
