//
//  PokemonViewController.swift
//  PruebaValid
//
//  Created by Juan martinez on 19/04/20.
//  Copyright © 2020 Juan Martinez. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD
import SwiftyJSON



class PokemonViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {

    

    @IBOutlet weak var PokemonTableView: UITableView!
    @IBOutlet weak var ViewSearch: UIView!
        
    
    let searchController = UISearchController ( searchResultsController : nil )
    
    var poke_data: Array<Pokemon> = []
    var poke_search: Array<Pokemon> = []
    var netx_poke: Array<Pokemon> = []
    var hud = JGProgressHUD(style: .dark)
    var data_result:Array<JSON> = []
    var data_next:Array<JSON> = []
    var nextUrl : String!
    var previousUrl : String!
    var total = 0
    var cant: Int!
    var numer = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        cant = 0
        cofg_search()
        get_result()
        PokemonTableView.delegate = self
        PokemonTableView.dataSource = self

    }
    
    func cofg_search(){
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.endEditing(true)
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.sizeToFit()
        
        ViewSearch.addSubview(searchController.searchBar)
    }
    
    

    // MARK: - Navigation
    
    func get_result(){

        Alamofire.request(url_pokemon,
                          method: .get,
                          encoding: URLEncoding.httpBody).responseJSON { response in
                            
                            self.hud.dismiss()
                            let datos = JSON(response.result.value!)
                            
                            self.data_result = datos["results"].arrayValue
                            self.nextUrl = datos["next"].stringValue
                            self.previousUrl = datos["previous"].stringValue
                            self.total = datos["count"].intValue
                            self.cant = datos["results"].count
                            
                            self.get_url(from: "initial")
                            
                            
                            
                            //self.PokemonTableView.reloadData()
                            
        }
        
        
    }
    
    
    func get_url(from:String){
        
        
        switch from {
        case "initial":
            
            for item in data_result {
                
                let url = item["url"].stringValue
                
                get_pokemon(url: url)
                
            }
            
        case "next":
            
            for item in data_next {
                
                let url = item["url"].stringValue
                get_next_pokemon(url: url)
                
            }

        default:
            _ = "no"
        }

        
        
    }
    
    func get_pokemon(url:String){
        
        numer = numer + 1
        
        hud.textLabel.text = "Un momento..."
        hud.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        hud.show(in: self.view)
        
        Alamofire.request(url,
                          method: .get,
                          encoding: URLEncoding.httpBody).responseJSON { response in
                                                        
                            
                            guard let data = response.data else { return }
                            
                            
                            do {
                                let u = try JSONDecoder().decode(Pokemon.self, from: data)
                                
                                self.poke_data.append(u)
                                
                            } catch let jsonErr {
                                print("Error serializing json:", jsonErr)
                                self.hud.dismiss()
                            }
                            
                            if self.numer == self.cant {
                                
                                self.PokemonTableView.reloadData()
                                self.hud.dismiss()
                            }
        
        }
        
    }
    
    func get_next_results(url:String){
        


        Alamofire.request(url,
                          method: .get,
                          encoding: URLEncoding.httpBody).responseJSON { response in
                            
                            self.hud.dismiss()
                            let datos = JSON(response.result.value!)
                            
                            self.data_next = datos["results"].arrayValue
                            self.nextUrl = datos["next"].stringValue
                            self.previousUrl = datos["previous"].stringValue
                            self.cant = self.cant + datos["results"].count
                            self.get_url(from: "next")
        }
    }
    
    func get_next_pokemon(url:String){
        
        numer = numer + 1
        hud.textLabel.text = "Un momento..."
        hud.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        hud.show(in: self.view)
        
        Alamofire.request(url,
                          method: .get,
                          encoding: URLEncoding.httpBody).responseJSON { response in
                                                        
                            
                            guard let data = response.data else { return }
                            
                            do {
                                let u = try JSONDecoder().decode(Pokemon.self, from: data)
                                self.poke_data.append(u)
                                
                            } catch let jsonErr {
                                print("Error serializing json:", jsonErr)
                                self.hud.dismiss()
                            }
                            
                            
                            if self.numer == self.cant {
                            
                                self.PokemonTableView.reloadData()
                                self.hud.dismiss()
                            }
        
        }
    }

    
    // MARK:-Table
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if searchController.isActive {
            return poke_search.count

        }else {
            
            return poke_data.count

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellPokemon") as? PokemonCollectionViewTable else { return UITableViewCell() }
        
        
        
        if searchController.isActive {
            
            guard let url = poke_search[indexPath.row].sprites?.frontDefault else { return cell }
            cell.img_pokemon.downloaded(from: url)
            cell.img_pokemon.contentMode = UIView.ContentMode.scaleAspectFill


            cell.txt_name.text = poke_search[indexPath.row].name
            cell.txt_id.text = "#" + poke_search[indexPath.row].id!.description
            
            let count_types = poke_search[indexPath.item].types?.count
            

            switch count_types {
            case 1:
                let img = self.type_img(tpye: (self.poke_search[indexPath.row].types?[0].type?.name)!)
                cell.img_type1.image = UIImage(named: img)
                cell.img_type2.isHidden = true

            case 2 :
                let img1 = self.type_img(tpye: (self.poke_search[indexPath.row].types?[1].type?.name)!)
                let img2 = self.type_img(tpye: (self.poke_search[indexPath.row].types?[0].type?.name)!)
                
                cell.img_type1.image = UIImage(named: img1)
                cell.img_type2.image = UIImage(named: img2)
                
            default:
                
                let img = self.type_img(tpye: (self.poke_search[indexPath.row].types?[0].type?.name)!)
                cell.img_type1.image = UIImage(named: img)
            }
            

        }else {
            guard let url = poke_data[indexPath.row].sprites?.frontDefault else { return cell }
            cell.img_pokemon.downloaded(from: url)
            cell.img_pokemon.contentMode = UIView.ContentMode.scaleAspectFill


            cell.txt_name.text = poke_data[indexPath.row].name
            cell.txt_id.text = "#" + poke_data[indexPath.row].id!.description
            
            let count_types = poke_data[indexPath.item].types?.count
            

            switch count_types {
            case 1:
                let img = self.type_img(tpye: (self.poke_data[indexPath.row].types?[0].type?.name)!)
                cell.img_type1.image = UIImage(named: img)
                cell.img_type2.isHidden = true

            case 2 :
                let img1 = self.type_img(tpye: (self.poke_data[indexPath.row].types?[1].type?.name)!)
                let img2 = self.type_img(tpye: (self.poke_data[indexPath.row].types?[0].type?.name)!)
                
                cell.img_type1.image = UIImage(named: img1)
                cell.img_type2.image = UIImage(named: img2)
                
            default:
                
                let img = self.type_img(tpye: (self.poke_data[indexPath.row].types?[0].type?.name)!)
                cell.img_type1.image = UIImage(named: img)
            }
            
            

        }

        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.init(named: "azul_select")
        cell.selectedBackgroundView = backgroundView

        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
                
        let name = poke_data[indexPath.row].name
        let img  = poke_data[indexPath.row].sprites?.frontDefault
        var types : Array<String> = []
        
        
        let count_types = poke_data[indexPath.item].types?.count
        
       

        switch count_types {
        case 1:
            let img = poke_data[indexPath.row].types?[0].type?.name
            
            types.append(img!)
        case 2 :
            let img1 = poke_data[indexPath.row].types?[1].type?.name
            let img2 = poke_data[indexPath.row].types?[0].type?.name
            
            types.append(img1!)
            types.append(img2!)

        default:
            let img = poke_data[indexPath.row].types?[0].type?.name
            types.append(img!)
        }
        
        
        

        
        
        
        if searchController.isActive {
            self.searchController.dismiss(animated: false){
                
                let storyboard = UIStoryboard (name: "Detail", bundle: nil)
                let resultVC = storyboard.instantiateViewController(withIdentifier: "DeatilView")as! DeatilPokemonViewController
                resultVC.name = name
                resultVC.img = img
                resultVC.type = types
                resultVC.modalPresentationStyle = .currentContext
                self.present(resultVC, animated: true, completion: nil)
                
            }
        }else {
            
            let storyboard = UIStoryboard (name: "Detail", bundle: nil)
            let resultVC = storyboard.instantiateViewController(withIdentifier: "DeatilView")as! DeatilPokemonViewController
            resultVC.name = name
            resultVC.img = img
            resultVC.type = types
            resultVC.modalPresentationStyle = .currentContext
            self.present(resultVC, animated: true, completion: nil)
        }
        
    }
    

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = poke_data.count - 1
        if indexPath.row == lastElement {
            
            if !searchController.isActive{
                
                if poke_data.count <= total && poke_data.count >= cant {
                    
                    get_next_results(url: nextUrl)

                }

            }
            

        }
    }
    
    
    
    func type_img(tpye: String) -> String {
        
        
        let img: String!
        
        switch tpye {
        
        case "bug":
             img = "Type_Bug"
        case "dark":
            img = "Type_Dark"
        case "dragon":
             img = "Type_Dragon"
        case "electric":
            img = "Type_Electric"
        case "fairy":
             img = "Type_Fairy"
        case "fight":
             img = "Type_Fight"
        case "fire":
             img = "Type_Fire"
        case "flying":
             img = "Type_Flying"
        case "ghost":
             img = "Type_Ghost"
        case "grass":
             img = "Type_Grass"
        case "ground":
             img = "Type_Ground"
        case "ice":
             img = "Type_Ice"
        case "normal":
             img = "Type_Normal"
        case "poison":
             img = "Type_Posion"
        case "psychic":
             img = "Type_Psychic"
        case "rock":
             img = "Type_Rock"
        case "steel":
             img = "Type_Steel"
        case "water":
             img = "Type_Water"
        default:
            img = "Type_Normal"
        }
        
        return img
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if searchController.searchBar.text! == "" {
            poke_search = poke_data
        } else {
            poke_search = poke_search.filter { ($0.name?.lowercased().contains(searchController.searchBar.text!.lowercased()))! }
        }
        
        self.PokemonTableView.reloadData()
    }
    
    



}
