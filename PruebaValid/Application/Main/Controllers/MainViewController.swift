//
//  MainViewController.swift
//  PruebaValid
//
//  Created by Juan martinez on 19/04/20.
//  Copyright © 2020 Juan Martinez. All rights reserved.
//

import UIKit



class MainViewController: UIViewController {
    
    
    @IBOutlet weak var Conten_view: UIView!
    @IBOutlet var buttons: [UIButton]!

    
    var pv: UIViewController!
    var mv: UIViewController!
    var iv: UIViewController!
    
    var viewControllers: [UIViewController]!
    
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        pv = storyboard.instantiateViewController(withIdentifier: "ViewPokemon")
        mv = storyboard.instantiateViewController(withIdentifier: "ViewMovies")
        iv = storyboard.instantiateViewController(withIdentifier: "ViewItems")
        viewControllers = [pv, mv, iv]
        
        buttons[selectedIndex].isSelected = true
        PressTab(buttons[selectedIndex])

    }
    
    @IBAction func PressTab(_ sender: UIButton) {
        
        let previousIndex = selectedIndex
        selectedIndex = sender.tag
        buttons[previousIndex].isSelected = false
        let previousVC = viewControllers[previousIndex]
        
        previousVC.willMove(toParent: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParent()
        
        sender.isSelected = true
        
        bnt_press(tag: sender.tag)
        
        let vc = viewControllers[selectedIndex]
        addChild(vc)
        vc.view.frame = Conten_view.bounds
        Conten_view.addSubview(vc.view)
        vc.didMove(toParent: self)

    }
    
    func bnt_press(tag : Int){
        
        switch tag {
        case 0:
            buttons[0].alpha = 1
            buttons[1].alpha = 0.5
            buttons[2].alpha = 0.5
            
        case 1:
            buttons[0].alpha = 0.5
            buttons[1].alpha = 1
            buttons[2].alpha = 0.5
        case 2:
            buttons[0].alpha = 0.5
            buttons[1].alpha = 0.5
            buttons[2].alpha = 1
        default:
            buttons[0].alpha = 1
            buttons[1].alpha = 0.5
            buttons[2].alpha = 0.5
            
        }
    }
    
    

    


}
