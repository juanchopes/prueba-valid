//
//  pokemon_model.swift
//  PruebaValid
//
//  Created by Juan martinez on 19/04/20.
//  Copyright © 2020 Juan Martinez. All rights reserved.
//

import Foundation


struct Pokemon : Decodable {

        let abilities : [Ability]?
        let baseExperience : Int?
        let forms : [Form]?
        let gameIndices : [GameIndex]?
        let height : Int?
        let heldItems : [HeldItem]?
        let id : Int?
        let isDefault : Bool?
        let locationAreaEncounters : String?
        let moves : [Move]?
        let name : String?
        let order : Int?
        let species : Species?
        let sprites : Sprite?
        let stats : [Stat]?
        let types : [Type]?
        let weight : Int?

        enum CodingKeys: String, CodingKey {
                case abilities = "abilities"
                case baseExperience = "base_experience"
                case forms = "forms"
                case gameIndices = "game_indices"
                case height = "height"
                case heldItems = "held_items"
                case id = "id"
                case isDefault = "is_default"
                case locationAreaEncounters = "location_area_encounters"
                case moves = "moves"
                case name = "name"
                case order = "order"
                case species = "species"
                case sprites = "sprites"
                case stats = "stats"
                case types = "types"
                case weight = "weight"
        }


}


struct HeldItem : Codable {

        let item : Item?
        let versionDetails : [VersionDetail]?

        enum CodingKeys: String, CodingKey {
                case item = "item"
                case versionDetails = "version_details"
        }
    
    struct VersionDetail : Codable {

            let rarity : Int?
            let version : Version?

            enum CodingKeys: String, CodingKey {
                    case rarity = "rarity"
                    case version = "version"
            }


    }
    
    struct Version : Codable {

            let name : String?
            let url : String?

            enum CodingKeys: String, CodingKey {
                    case name = "name"
                    case url = "url"
            }
    }
    
    struct Item : Codable {

            let name : String?
            let url : String?

            enum CodingKeys: String, CodingKey {
                    case name = "name"
                    case url = "url"
            }
    }

}




struct Type : Decodable {

        let slot : Int?
        let type : Type?

        enum CodingKeys: String, CodingKey {
                case slot = "slot"
                case type = "type"
        }
    
    
    struct `Type` : Decodable {

            let name : String?
            let url : String?

            enum CodingKeys: String, CodingKey {
                    case name = "name"
                    case url = "url"
            }
        
    }

}



struct Stat : Decodable {

        let baseStat : Int?
        let effort : Int?
        let stat : Stat?

        enum CodingKeys: String, CodingKey {
                case baseStat = "base_stat"
                case effort = "effort"
                case stat = "stat"
        }
    

    struct Stat : Decodable {

            let name : String?
            let url : String?

            enum CodingKeys: String, CodingKey {
                    case name = "name"
                    case url = "url"
            }
        

    }

}



struct Sprite : Decodable {

        let backDefault : String?
        let backFemale : String?
        let backShiny : String?
        let backShinyFemale : String?
        let frontDefault : String?
        let frontFemale : String?
        let frontShiny : String?
        let frontShinyFemale : String?

        enum CodingKeys: String, CodingKey {
                case backDefault = "back_default"
                case backFemale = "back_female"
                case backShiny = "back_shiny"
                case backShinyFemale = "back_shiny_female"
                case frontDefault = "front_default"
                case frontFemale = "front_female"
                case frontShiny = "front_shiny"
                case frontShinyFemale = "front_shiny_female"
        }
    

}


struct Species : Decodable {

        let name : String?
        let url : String?

        enum CodingKeys: String, CodingKey {
                case name = "name"
                case url = "url"
        }

}

struct Move : Decodable {

        let move : Move?
        let versionGroupDetails : [VersionGroupDetail]?

        enum CodingKeys: String, CodingKey {
                case move = "move"
                case versionGroupDetails = "version_group_details"
        }
    
    
    struct Move : Decodable {

            let name : String?
            let url : String?

            enum CodingKeys: String, CodingKey {
                    case name = "name"
                    case url = "url"
            }
        

    }

}



struct VersionGroupDetail : Decodable {

        let levelLearnedAt : Int?
        let moveLearnMethod : MoveLearnMethod?
        let versionGroup : VersionGroup?

        enum CodingKeys: String, CodingKey {
                case levelLearnedAt = "level_learned_at"
                case moveLearnMethod = "move_learn_method"
                case versionGroup = "version_group"
        }
    

}

struct VersionGroup : Decodable {

        let name : String?
        let url : String?

        enum CodingKeys: String, CodingKey {
                case name = "name"
                case url = "url"
        }

}

struct MoveLearnMethod : Decodable {

        let name : String?
        let url : String?

        enum CodingKeys: String, CodingKey {
                case name = "name"
                case url = "url"
        }
    

}

struct GameIndex : Decodable {

        let gameIndex : Int?
        let version : Version?

        enum CodingKeys: String, CodingKey {
                case gameIndex = "game_index"
                case version = "version"
        }
    
}

struct Version : Decodable {

        let name : String?
        let url : String?

        enum CodingKeys: String, CodingKey {
                case name = "name"
                case url = "url"
        }
    


}



struct Form : Decodable {

        let name : String?
        let url : String?

        enum CodingKeys: String, CodingKey {
                case name = "name"
                case url = "url"
        }
    
}

struct Ability : Decodable {

        let ability : Ability?
        let isHidden : Bool?
        let slot : Int?

        enum CodingKeys: String, CodingKey {
                case ability = "ability"
                case isHidden = "is_hidden"
                case slot = "slot"
        }
    
    
    
    struct Ability : Decodable {

            let name : String?
            let url : String?

            enum CodingKeys: String, CodingKey {
                    case name = "name"
                    case url = "url"
            }
        

    }

}



