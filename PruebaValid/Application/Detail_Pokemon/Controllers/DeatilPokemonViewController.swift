//
//  DeatilPokemonViewController.swift
//  PruebaValid
//
//  Created by Juan martinez on 20/04/20.
//  Copyright © 2020 Juan Martinez. All rights reserved.
//

import UIKit

class DeatilPokemonViewController: UIViewController {
    
    
    @IBOutlet weak var ViewGradient: UIView!
    @IBOutlet weak var txt_name: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var img_pokemon: UIImageView!
    @IBOutlet weak var viewCorner: UIView!
    @IBOutlet weak var viewType: UIView!
    
    
    var name : String!
    var img : String!
    var type : Array<String> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewCorner.roundCorners(corners:  [.topLeft, .topRight], radius: 48)
        ViewGradient.backgroundColor = UIColor(named: type[0])
        fill()
    }
    
    
    func fill (){
        txt_name.text = name
        img_pokemon.downloaded(from: img)
        img_pokemon.contentMode = UIView.ContentMode.scaleToFill
        
        switch type.count {
            
        case 1:
            let img1 = type_tag(tpye: type[0])
            let image = UIImage(named: img1)
            let imageView = UIImageView(image: image!)
            imageView.center = CGPoint(x: viewType.frame.size.width / 2, y: viewType.frame.size.height / 2)
            
            viewType.addSubview(imageView)
        case 2:
            
            
            let img1 = type_tag(tpye: type[0])
            let image1 = UIImage(named: img1)
            let imageView1 = UIImageView(image: image1)
            
            imageView1.center = CGPoint(x: viewType.frame.size.width / 1.5, y: viewType.frame.size.height / 2)

            viewType.addSubview(imageView1)
            
            let img2 = type_tag(tpye: type[1])
            
            let image2 = UIImage(named: img2)
            let imageView2 = UIImageView(image: image2!)
            imageView2.center = CGPoint(x: viewType.frame.size.width / 3, y: viewType.frame.size.height / 2)

            
            viewType.addSubview(imageView2)

            
            
        default:
            let img1 = type_tag(tpye: type[0])
            let image = UIImage(named: img1)
            let imageView = UIImageView(image: image!)
            viewType.addSubview(imageView)
        }
        

    }
    
    @IBAction func Go_back(_ sender: UIButton) {
        
        navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    
    func type_tag(tpye: String) -> String {
        
        
        let img: String!
        
        switch tpye {
        
        case "bug":
             img = "Tag_Bug"
        case "dark":
            img = "Tag_Dark"
        case "dragon":
             img = "Tag_Dragon"
        case "electric":
            img = "Tag_Electric"
        case "fairy":
             img = "Tag_Fairy"
        case "fight":
             img = "Tag_Fight"
        case "fire":
             img = "Tag_Fire"
        case "flying":
             img = "Tag_Flying"
        case "ghost":
             img = "Tag_Ghost"
        case "grass":
             img = "Tag_Grass"
        case "ground":
             img = "Tag_Ground"
        case "ice":
             img = "Tag_Ice"
        case "normal":
             img = "Tag_Normal"
        case "poison":
             img = "Tag_Poison"
        case "psychic":
             img = "Tag_Psychic"
        case "rock":
             img = "Tag_Rock"
        case "steel":
             img = "Tag_Steel"
        case "water":
             img = "Tag_Water"
        default:
            img = "Tag_Normal"
        }
        
        return img
    }

}
